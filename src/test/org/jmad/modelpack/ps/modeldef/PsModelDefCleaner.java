/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.jmad.modelpack.ps.modeldef;

import static cern.accsoft.steering.jmad.tools.modeldefs.cleaning.ModelPackageCleaner.cleanUnusedBelow;

public class PsModelDefCleaner {

    public static void main(String[] args) {
        cleanUnusedBelow("src/java");
    }

}
