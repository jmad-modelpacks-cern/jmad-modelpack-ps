/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.jmad.modelpack.ps.modeldef;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class PsModelDefCreator {

    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/org/jmad/modelpack/ps/modeldef/defs");
    }

}
