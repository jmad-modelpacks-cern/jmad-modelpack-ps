/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.jmad.modelpack.ps.modeldef.defs;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

public class PsModelFactory extends JMadModelDefinitionDslSupport {

    private static final String[] NON_DEFAULT_OPTICS_NAMES = { "PS_EXT_LHC", "PS_EXT_TOF", "PS_FT_LHC", "PS_FT_SFTPRO",
            "PS_FT_SFTPRO_C800", "PS_FT_TOF", "PS_INJ_LHC", "PS_LE_AD", "PS_LE_SFTPRO", "PS_LE_TOF",
            "PS_LE_bare_machine" };
    private static final String DEFAULT_OPTICS = "PS_LE_LHC";

    {
        name("PS");

        offsets(o -> {
            o.repository("2018");
        });

        init(i -> {
            i.call("elements/PS.ele");
            i.call("beams/ps_beam_1dot4GeV.beamx");
            i.call("sequence/PS.seq");
            i.call("strength/PS_LE_LHC.str"); // is this really necessary?
            i.call("strength/elements.str").parseAs(STRENGTHS);
        });

        sequence("ps").isDefault().isDefinedAs(s -> {
           s.range("ALL").isDefault().isDefinedAs(r -> {
               r.twiss(t -> {
                   t.calcAtCenter();
               });
           }); 
        });

        optics(DEFAULT_OPTICS).isDefault().isDefinedAs(o -> {
            o.call(strengthFileNameFor(DEFAULT_OPTICS)).parseAs(STRENGTHS);
        });

        for (String opticsName : NON_DEFAULT_OPTICS_NAMES) {
            optics(opticsName).isDefinedAs(o -> {
                o.call(strengthFileNameFor(opticsName)).parseAs(STRENGTHS);
            });
        }

    }

    private String strengthFileNameFor(String opticsName) {
        return "strength/" + opticsName + ".str";
    }
}
